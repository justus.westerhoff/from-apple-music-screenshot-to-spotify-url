from flask import Flask, request
from twilio.twiml.messaging_response import MessagingResponse
import os
import requests
import time
from vision import get_meta_infos
from api import *

app = Flask(__name__)  # initiate flask app

def respond(message):
        response = MessagingResponse()
        response.message(message)
        return str(response)

# Our '/bot' route and this will only listen to POST requests (received from twilio)
@app.route("/bot", methods=['POST'])
def bot():
        print("[INFO] message received!")
        sender = request.form.get('From')
        #message = request.form.get('Body') # we are not interested in the transmitted text at the moment
        media_url = request.form.get('MediaUrl0') # contains the url to the transmitted image

        if media_url:
                r = requests.get(media_url) # this is of type <class 'requests.models.Response'>
                content_type = r.headers['Content-Type']
                username = sender.split(':')[1] # remove the whatsapp: prefix from the number
                #username = hash(username) # hashing the username (=telephone number) for privacy reasons
                message = time.strftime("%Y_%m_%d-%H_%M_%S") # add time stamp to message (message is empty at the moment)
                if content_type == 'image/jpeg':
                        filename = f'uploads/{username}/{message}/received_screenshot.jpg'
                elif content_type == 'image/png':
                        filename = f'uploads/{username}/{message}/received_screenshot.png'
                else:
                        filename = None
                if filename:
                        if not os.path.exists(f'uploads/{username}/{message}'):
                                print("[INFO] creating directory...")
                                os.makedirs(f'uploads/{username}/{message}') # to create intermediate folders
                        # saving image
                        print("[INFO] saving received image...")
                        try:
                                with open(filename, 'wb') as file: # 'wb' for write ('rb' for read)
                                        result = file.write(r.content)
                                        file.close()
                        except IOError as x:
                                if x.errno == errno.ENOENT:
                                        print(filename, '- does not exist')
                                elif x.errno == errno.EACCES:
                                        print(filename, '- cannot be read')
                                else:
                                        print(filename, '- some other error')

                        # processing image. TODO: test for different devices etc.
                        songname, singer = get_meta_infos(filename)  # filename is the name to the previously written file
                        spotify_url = api_call(songname, singer)
                        # TODO: also get Apple Music URL

                        # saving the calculated information
                        print("[INFO] saving information.txt...")
                        information = open(f'uploads/{username}/{message}/information.txt', 'w')
                        information.write("songname: " + songname + "\nsinger: " + singer + "\nSpotify url: " + spotify_url)
                        information.close()
                        

                        # saving clickable link to the song
                        print("[INFO] saving song.url...")
                        link = open(f'uploads/{username}/{message}/song.url', 'w')
                        link.write("[InternetShortcut]\nURL = " + spotify_url)
                        link.close()


                        print('[INFO] sending response...')
                        return respond('Track *%s* by *%s*. Spotify URL: %s'%(songname,singer,spotify_url))
                else:
                        return respond('The file that you submitted is not a supported image type. Use JPEG or PNG.')
        else:
                return respond(f'Please send a screenshot of a playing Apple Music track!')

if __name__ == "__main__":
        app.run(debug=True)

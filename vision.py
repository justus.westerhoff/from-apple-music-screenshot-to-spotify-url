import pytesseract
from PIL import Image

def get_meta_infos(path): # path to the file
        image = Image.open(path)
        w, h = image.size
        x = int(0.08 * w)
        new_w = int(0.75 * w)
        y = int(0.615 * h)
        new_h = int(0.08 * h)
        border = (x, y, x+new_w, y+new_h)
        # this border extracts the part of the screenshot
        # that contains the track information (see README.md)
        image = image.crop(border)

        # saving cropped image
        # remove last 4 digits of path string, i.e., its end, and add it after
        # we added "_cropped"
        filename =  path[:-4] + "_cropped" + path[-4:]
        image.save(filename)
        print("[INFO] saved cropped image")

        text = pytesseract.image_to_string(image)
        arr = text.splitlines()
        songname = arr[0]
        singer = arr[1]
        return songname, singer

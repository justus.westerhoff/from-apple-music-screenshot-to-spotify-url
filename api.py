import requests
import base64
import json
import time
import os, sys

# global variables
CLIENT_ID = '59e80e33974d4ebb8d9a4190c170e6e7'
CLIENT_SECRET = 'c6a81201b9c0473ca77db40cd191d106'
AUTH_URL = 'https://accounts.spotify.com/api/token'

AUTH_TOKEN = '' # filled in by get_token()
AUTH_TIME = 0 # time when AUTH_TOKEN was received (an AUTH_TOKEN is valid for 3600 seconds)

def get_token():
    # thanks to https://dev.to/mxdws/using-python-with-the-spotify-api-1d02
    headers = {}
    data = {}

    # Encode as Base64
    message = f"{CLIENT_ID}:{CLIENT_SECRET}"
    messageBytes = message.encode('ascii')
    base64Bytes = base64.b64encode(messageBytes)
    base64Message = base64Bytes.decode('ascii')

    headers['Authorization'] = f"Basic {base64Message}"
    data['grant_type'] = "client_credentials"

    print('[INFO] requesting Spotify API auth key...')
    auth_response = requests.post(AUTH_URL, headers=headers, data=data)
    # TODO: throw error if auth_reponse is not <Response [200]>
    global AUTH_TOKEN
    AUTH_TOKEN = auth_response.json()['access_token']
    global AUTH_TIME
    AUTH_TIME = time.time()

def api_call(songname, singer):
    if time.time() - AUTH_TIME > 3500:
        get_token()

    search = (songname + " " + singer).replace(" ", "%20")
    auth = "Bearer " + AUTH_TOKEN
    print('[INFO] searching for track %s by %s...'%(songname, singer))
    api_response = requests.get("https://api.spotify.com/v1/search?q=" + search + "&type=track",
                                        headers={"Authorization": auth})

    # DEBUG space:
    # with open("DEBUG_api_response_text.txt", 'w') as file:
    #     file.write('\n'.join(api_response.text.split("/n")))
    # I found out that the old procedure wasn't good enough.
    # We should look for a url where the "type" is "track" and which
    # has the exact same name that we are looking for
    # the IDEA now is to just take the first 'open.spotify' URL with "type": "track"
    # (relying on the assumption that the first one of those is the one that fits best)
    # also, we are now handling the Response.text as a dict, using json
    dict = json.loads(api_response.text)
    # with open("DEBUG_data_api_response_text.json", 'w') as file:
    #     json.dump(dict, file, indent=4)
    spotify_url = dict["tracks"]["items"][0]["external_urls"]["spotify"]
    return spotify_url

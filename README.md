# "Simplifying" communication between Apple Music and Spotify users

## What's this?
This WhatsApp bot takes a screenshot of a song in Apple Music via a private WhatsApp message and returns a Spotify link to the particular song in the image. [TODO] Additionally, it also sends the corresponding Apple Music link.

## For the end user
First of all, send `join verb-voice` to [+14155238886](tel:+14155238886) via WhatsApp. This allows the bot to send you messages (it will only send you the above mentioned reply once you send a screenshot - no spam, I swear). Then you can go on and send him screenshots of Apple Music songs; like this one:

<img src="./documentation/example.jpg" width="100">

Feel free to use this. Though I don't guarantee for it to be working and always being online.

# For the provider

## How to install
Clone this repo and run

`virtualenv -p /usr/bin/python3 env && source env/bin/activate && pip3 install -r requirements.txt`

(as explained [here](https://stackoverflow.com/a/12657803/13153367))

Also, install *ngrok*. For example via `brew install --cask ngrok` on Mac or via `sudo snap install ngrok` on Ubuntu (for Raspberry OS, see [here](https://snapcraft.io/install/ngrok/raspbian)). (Recall that with *snap*, you run things with `snap run ...`.)

**ON UBUNTU, I also needed to `sudo apt-get install tesseract-ocr`.**

## How to set up
First, you need to create an app at [Spotify for developers](https://developer.spotify.com/) to access their API without
manually getting tokens every hour by hand. You will find your (permanent) *Client ID* and *Client Secret* [there](https://developer.spotify.com/dashboard/).
These will be used to fetch authentification tokens.
Open `api.py` and fill your *Client ID* and *Client Secret* in.

For the communication via WhatsApp, [twilio](https://www.twilio.com/) is used. 
You can create an account for free and will have (as of today), around 15$ of starting credit. 
Both an inbound and outbound message cost $0.005 each, so this credit should probably suffice for a long time.

To make the local *flask* app accessible to *twilio*, you will need a tunnel to your localhost. 
For example, you can use [ngrok](https://ngrok.com/).
After creating an account here, you will get a permanent token (see [here](https://ngrok.com/docs#authtoken)).
You can then find your token [here](https://dashboard.ngrok.com/get-started/your-authtoken).
Run `ngrok authtoken yourauthtoken`
Your *ngrok* sessions will now not be time-limited.

## How to start the app itself
- If not already done so, activate the virtual environment with `source env/bin/activate` and run `python3 app.py`. This starts the flask webserver that establishes a connection for twilio to our local host that the received messages from the end user and sends the appropriate response.
- In a new terminal, run `ngrok http 5000` to make the local flask accessible to the internet.
- In the above step, you will find a *Forwarding* link. Insert the link including "/bot" at the end 
in the *twilio* [message control center](https://console.twilio.com/us1/develop/sms/settings/whatsapp-sandbox?frameUrl=%2Fconsole%2Fsms%2Fwhatsapp%2Fsandbox%3Fx-target-region%3Dus1) under *Messaging/Settings/Whatsapp sandbox settings*
at *WHEN A MESSAGE COMES IN* and click *SAVE*. The link you have to put in should look something like this:
http://1bde-32-3-251-123.ngrok.io/bot

The process described in "For the end user" should now work. You should of course replace the telephone number and sandbox name
by the ones *twilio* assigned to you. These can also be found in the [message control center](https://console.twilio.com/us1/develop/sms/settings/whatsapp-sandbox?frameUrl=%2Fconsole%2Fsms%2Fwhatsapp%2Fsandbox%3Fx-target-region%3Dus1).

### Credits
The general idea comes from https://decodebuzzing.medium.com/whatsapp-bot-that-detects-chat-messages-and-joins-your-zoom-meetings-2b035776f9c7

The handling of WhatsApp images is explained in https://www.twilio.com/blog/receive-images-whatsapp-flask-twilio

# TODOS
- the `join verb-voice` at the *twilio* number only "authenticates" for 72hrs. Is there a way to make it permanent? Maybe through Twilio payment...
- implement auto start at reboot. E.g. via [cron](https://wiki.ubuntuusers.de/Cron/). Problem: *ngrok* gives new URL on every restart...
["On the free plan, ngrok's URLs are randomly generated and temporary. If you want to use the same URL every time, you need to upgrade to a paid plan"](https://ngrok.com/docs#:~:text=Getting%20a%20stable%20URL,stable%20address%20with%20TCP%20tunnels).
There *were* free alternatives with stable links, e.g., [localhost.run](http://localhost.run/), but those are now paid services as well.
- make a *cron* job that "validates" the activity of the bot, e.g. by
    - pinging the *ngrok* link
    - sending a post request to the bot
    - checking if on a standardized screenshot the correct link is being sent back

# Different approaches
- get bot directly into group somehow -- maybe through RPi SIM adapter and prepaid card?! WhatsApp API necessary then -- availability? Does not look good...
- another totally different idea: do all the work immediately on your own phone
    - need an app that "listens" to selected WhatsApp chats and can handle code
        - that does not look good... [uff](https://github.com/venomous0x/WhatsAPI). the issue here is something else though; for this approach we would not really need an API
    - still, I think this is our best lead:
        - find out how to write your own simple Android app, let this listen to specific WhatsApp chats and also send back messages (or, in a first step, only receive and then forward to the RPi via `ngrok` and receive result via *Twilio*; in a second step we would of course want to completely eliminate *Twilio* and any forwarding)
            - start research [here](https://stackoverflow.com/questions/32309061/sending-message-to-whatsapp-from-android-app) and [here](https://developer.android.com/training/sharing/send)
- replace Twilio by Telegram if there are easy programmable bots *for free* in Telegram
    - this does not replace the forwarding of messages, but there is no need for any authentification (like `join verb-voice` above) anymore
